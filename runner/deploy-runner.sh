#!/bin/bash

helm upgrade --install --namespace default  \
             -f values.yaml       \
             gitlab-runner gitlab/gitlab-runner

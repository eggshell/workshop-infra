#!/usr/bin/env python3

import gitlab

# python-gitlab config must be in user's homedir
gl = gitlab.Gitlab.from_config('vm', ['.python-gitlab.cfg'])
users_amount = 200

for i in range(1, users_amount+1):
    i_string = str(i)

    try:
        search_string = 'workshop-user' + i_string
        user = gl.users.list(search=search_string)
        if user:
            print("deleting user: " + "workshop-user" + i_string)
            gl.users.delete(user[0].id, hard_delete=True)
        else:
            pass
    except gitlab.exceptions.GitlabCreateError as e:
        print(e)
        pass

# workaround to fix first two users not getting deleted
user1 = gl.users.list(search="workshop-user1", hard_delete=True)
gl.users.delete(user1[0].id)
user2 = gl.users.list(search="workshop-user2", hard_delete=True)
gl.users.delete(user2[0].id)

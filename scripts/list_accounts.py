#!/usr/bin/env python3

import gitlab
from os.path import expanduser

# python-gitlab config must be in user's homedir
gl = gitlab.Gitlab.from_config('vm', [expanduser('~' + '/.python-gitlab.cfg')])

print(gl.users.list())
